import attr
import yaml
import json
import ujson
import spotipy
import logging
from tinydb import TinyDB, Query 
from spotipy.util import prompt_for_user_token


def load_config_file(path):

    with open(path, 'r') as ymlfile:
        config = yaml.load(ymlfile)

    return config

config = load_config_file('config.yaml')


@attr.s
class Song: 
    name = attr.ib()
    artist = attr.ib()
    album = attr.ib()
    track_number = attr.ib()
    spotify_id = attr.ib()


class Library:

    def __init__(self, database_file):
        self.database_file = database_file
        self.db = TinyDB(database_file)
        self.db_query = Query()

        self.require_token() 
        self.create_spotipy_instance()

    def require_token(self):

        self.token = prompt_for_user_token(config['spotify']['username'],
                                           client_id=config['spotify']['client_id'],
                                           client_secret=config['spotify']['client_secret'],
                                           redirect_uri=config['spotify']['redirect_url'],
                                           scope='user-library-read')

        if not self.token:
            raise "Can't get token. Fuck."

    def create_spotipy_instance(self):

        if not self.token:
            self.require_token()

        self.spotipy_instance = spotipy.Spotify(auth=self.token)


    def sync(self):
        print('Started sync of playlist "{}".'.format(config['spotify']['target_playlist']))

        playlists = self.spotipy_instance.user_playlists(config['spotify']['username'])
        results = self.spotipy_instance.current_user_saved_tracks()
        
        target_playlist = [playlist['id'] for playlist in playlists['items']
                           if playlist['name'] == config['spotify']['target_playlist']]

        target_playlist_id = target_playlist[0]

        results = self.spotipy_instance.user_playlist(config['spotify']['username'],
                                                      target_playlist_id,
                                                      fields="tracks,next")

        tracks = results['tracks']

        number_of_synced_songs = 0

        while tracks['next']:
            tracks = self.spotipy_instance.next(tracks)

            for i, item in enumerate(tracks['items']):
                track = item['track']

                track_object = Song(name = track['name'],
                                    album = track['album']['name'],
                                    track_number = track['track_number'],
                                    artist = track['artists'][0]['name'],
                                    spotify_id = track['id'])

                if not self.db.search(self.db_query.spotify_id == track_object.spotify_id):

                    self.db.insert(attr.asdict(track_object))

                    print(track_object)

                    number_of_synced_songs += 1

        print("Sync resulted in {} songs being added.".format(number_of_synced_songs))

    def stats(self):
        print("{} songs in database.".format(len(self.db.all())))

def main():

    library = Library(config['database']['file'])
    library.sync()
    library.stats()

if __name__ == "__main__":
    main()
